-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
eval (Const num) = num
eval (Add t1 t2) = eval t1 + eval t2
eval (Sub t1 t2) = eval t1 - eval t2
eval (Mult t1 t2) = eval t1 * eval t2

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Const a) = Const a
simplify (Add t1 t2) = Add (simplify t1) (simplify t2)
simplify (Sub t1 t2) = Add (simplify t1) (simplify (negative t2))
simplify (Mult t1 t2) = Add (Mult (simplify t1) t2) (Mult (simplify t2) t1)

